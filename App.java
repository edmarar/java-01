import controle.ControleAluno;
import modelo.Aluno;

public class App {
	public static void main(String[] args) {
		
		// 1. ADICIONAR
		
		/* Aluno ze = new Aluno();
		ControleAluno cont = new ControleAluno();
		ze.setNumero(88);
		ze.setNome("Zezin");
		ze.dataNascimento.set(2003, 4, 14);
		ze.setHobby("Dormir de cama");
		ze.setSexo("M");
		if(cont.inserir(ze) ) {
			System.out.println("[+] Adicionado com sucesso.");
		}else {
			System.out.println("[-] Erro ao adicionar.");
		} */
		
		// 2. DELETAR 
		
		/* ControleAluno cont = new ControleAluno();
		int numero;
		numero = 100;
		if(cont.deletar(numero)) {
			System.out.println("[+] Deletado com sucesso.");
		} else {
			System.out.println("[-] Erro ao deletaar.");
		} */
		
		// 3. ATUALIZAR
		
		Aluno ze = new Aluno();
		ControleAluno cont = new ControleAluno();
		int numero = 88;
		ze.setNumero(numero);
		ze.setNome("Bichin");
		ze.dataNascimento.set(2003, 4, 14);
		ze.setHobby("Dormir de cama");
		ze.setSexo("M");
		if(cont.atualizar(ze, numero) ) {
			System.out.println("[+] Atualizado com sucesso.");
		}else {
			System.out.println("[-] Erro ao atualizar.");
		}
		
	}

}
