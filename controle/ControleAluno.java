package controle;
import modelo.Aluno;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
public class ControleAluno {
	public boolean inserir(Aluno al) {
		boolean resultado = false;
		Conexao con = new Conexao();
		try {
			String comando = "INSERT INTO usuarios VALUES(?,?,?,?,?);";
			PreparedStatement ps = con.getCon().prepareStatement(comando);
			ps.setInt(1, al.getNumero());
			ps.setString(2, al.getNome());
			String data = al.dataNascimento.toInstant().toString().substring(0, 10);
			Date dataSQL = Date.valueOf(data);
			ps.setDate(3, dataSQL);
			ps.setString(4, al.getHobby());
			ps.setString(5, al.getSexo());
			if(!ps.execute()) {
				resultado = true;
			}
		}catch(SQLException e){
			System.out.println("[-] Erro ao Inserir");
			System.out.println(e.getMessage());
		}finally {
			con.fecharConexao();
		}
		return resultado;
	} 
	
	public boolean deletar(int al) {
		boolean resultado = false;
		Conexao con = new Conexao();
		try {
			String comando = "DELETE FROM usuarios WHERE numero=?;";
			PreparedStatement ps = con.getCon().prepareStatement(comando);
			ps.setInt(1, al);
			if(!ps.execute()) {
				resultado = true;
			}
		}catch(SQLException e){
			System.out.println("[-] Erro ao Deletar");
			System.out.println(e.getMessage());
		}finally {
			con.fecharConexao();
		}
		return resultado;
	}
	
	public boolean atualizar(Aluno al, int numero) {
		boolean resultado = false;
		Conexao con = new Conexao();
		try {
			String comando = "UPDATE usuarios SET nome=?, dataNascimento=?, hobby=?, sexo=? WHERE numero=?;";
			PreparedStatement ps = con.getCon().prepareStatement(comando);
			ps.setInt(5, numero);
			ps.setString(1, al.getNome());
			String data = al.dataNascimento.toInstant().toString().substring(0, 10);
			Date dataSQL = Date.valueOf(data);
			ps.setDate(2, dataSQL);
			ps.setString(3, al.getHobby());
			ps.setString(4, al.getSexo());
			if(!ps.execute()) {
				resultado = true;
			}
		}catch(SQLException e){
			System.out.println("[-] Erro ao Inserir");
			System.out.println(e.getMessage());
		}finally {
			con.fecharConexao();
		}
		return resultado;
	}
}
