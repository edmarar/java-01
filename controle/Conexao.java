package controle;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class Conexao {
	private Connection con = null;
	// Abrir a conexao
	public Conexao() {
		try{
			Class.forName("com.mysql.cj.jdbc.Driver");
			String usuario = "root";
			String senha = "";
			String banco = "ajax";
			String servidor = "jdbc:mysql://localhost/" + banco;
			this.con = DriverManager.getConnection(servidor,usuario,senha);
			System.out.println("[+] Conexão efetuada com sucesso");
		}catch(ClassNotFoundException e) {
			System.out.println("[-] Verifique a biblioteca do JDBC");
		}catch(SQLException e){
			System.out.println("[-] Erro ao conectar no banco");
		}catch(Exception e){
			System.out.println("[-] Erro geral: " + e.getMessage());
		}
	}
	public Connection getCon() {
		return this.con;
	}
	public void fecharConexao() {
		try {
			this.con.close();
		}catch(SQLException e) {
			System.out.println("[-] Erro ao fechar a conexão.");
		}
	}
	
}
